import * as React from 'react';

interface IProps {
    onShot: (x: number, y: number) => void
    color: string
    x: number
    y: number
}

export default function Cell (props: IProps) {
    const { x, y, color, onShot } = props;

    return (
        <div onClick={() => onShot(x, y)} className="board-cell" style={{backgroundColor: color}} />
    )
}
