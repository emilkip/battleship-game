import * as React from "react";

interface IProps {
    status: any
}

interface IState {
    status: any
}

export default class Score extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            status: props.status || {},
        };
    }

    static getDerivedStateFromProps(nextProps: IProps, prevState: IState): IState {
        return {
            status: nextProps.status || {}
        }
    }

    render() {
        return (
            <div className="score">
                <h3>Score</h3>
                <hr/>
                <ul>
                    <li><b>Battleship:</b> {this.state.status[1]}</li>
                    <li><b>Cruiser:</b> {this.state.status[2]}</li>
                    <li><b>Boat:</b> x{this.state.status[3]}</li>
                </ul>
            </div>
        );
    }
}
