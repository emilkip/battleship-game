import * as React from 'react';
import GameGrid from '../components/GameGrid';
import Score from '../components/Score';
import {constants} from "../configs/constants";
import 'styleAlias/style.scss';


interface IState {
    board: number[][]
    shipStatus: any
}

export default class Game extends React.Component<any, IState> {
    constructor(props: any) {
        super(props);
        this.state = {
            board: [],
            shipStatus: {
                1: 4,
                2: 4,
                3: 2
            }
        };
    }

    componentDidMount() {
        this.initField(constants.DIMENSION);
    }

    initField = (size: number): void => {
        const board: number[][] = [];
        for(let i = 0; i < size; i++) {
            board.push([]);
            for(let j = 0; j < size; j++) {
                board[i][j] = 0;
            }
        }

        this.setState({
            board
        }, () => {
            this.generateShips();
        });
    };

    generateShips = (): void => {
        for(let i = 0; i < constants.SHIP_NUM; i++) {
            let isFitted = false;

            while(!isFitted) {
                isFitted = this.putShip(constants.SHIPS_TO_GENERATE[i]);
            }
        }
    };

    checkSurroundCells = (y: number, x: number): boolean => {
        if (x < 0 || y < 0 || x >= constants.DIMENSION || y >= constants.DIMENSION) return false;

        if (this.state.board[y][x] > 0 ||
            this.state.board[y][x + 1] > 0 ||
            this.state.board[y][x - 1] > 0 ||
            (y !== 9 && this.state.board[y + 1][x] > 0) ||
            (y !== 9 && this.state.board[y + 1][x + 1] > 0) ||
            (y !== 9 && this.state.board[y + 1][x - 1] > 0) ||
            (y !== 0 && this.state.board[y - 1][x] > 0) ||
            (y !== 0 && this.state.board[y - 1][x + 1] > 0) ||
            (y !== 0 && this.state.board[y - 1][x - 1] > 0)) {
            return false;
        }
        return true;
    };

    checkSpaceForShip = (x: number, y: number, size: number, direction: number, type: string): boolean => {
        for(let i = 0; i < size; i++) {
            if (!this.checkSurroundCells(y, x)) return false;

            if (i === size - 2 && type === 'L') {
                if (direction === 0) y--;
                if (direction === 1) x++;
                if (direction === 2) y++;
                if (direction === 3) x--;
            } else {
                if (direction === 0) x++;
                if (direction === 1) y++;
                if (direction === 2) x--;
                if (direction === 3) y--;
            }
        }
        return true;
    };

    setShipToBoard = (x: number, y: number, size: number, direction: number, type: string): void => {
        let board: number[][] = this.state.board;
        for(let i = 0; i < size; i++) {
            board[y][x] = constants.SHIP_TYPES[type];

            if (i === size - 2 && type === 'L') {
                if (direction === 0) y--;
                if (direction === 1) x++;
                if (direction === 2) y++;
                if (direction === 3) x--;
            } else {
                if (direction === 0) x++;
                if (direction === 1) y++;
                if (direction === 2) x--;
                if (direction === 3) y--;
            }
        }

        this.setState({
            board
        });
    };

    generateCurveShip = (x: number, y: number, direction: number, type: string): boolean => {
        let canPlace: boolean = this.checkSpaceForShip(x, y, 4, direction, type);

        if (canPlace) {
            this.setShipToBoard(x, y, 4, direction, 'L')
        }
        return canPlace;
    };

    generateRegularShip = (x: number, y: number, size: number, direction: number, type: string): boolean => {
        let canPlace: boolean = this.checkSpaceForShip(x, y, size, direction, type);

        if (canPlace) {
            this.setShipToBoard(x, y, size, direction, type)
        }
        return canPlace;
    };

    putShip = (type: string): boolean => {
        const randX: number = Math.floor(Math.random() * constants.DIMENSION - 1);
        const randY: number = Math.floor(Math.random() * constants.DIMENSION - 1);
        const direction: number = Math.floor(Math.random() * 4);

        if (constants.SHIP_TYPES[type] === 1) return this.generateCurveShip(randX, randY, direction, 'L');
        if (constants.SHIP_TYPES[type] === 2) return this.generateRegularShip(randX, randY, 4, direction, 'I');
        if (constants.SHIP_TYPES[type] === 3) return this.generateRegularShip(randX, randY, 1, direction, 'B');
        return true;
    };

    onShot = (x: number, y: number): void => {
        const board: number[][] = this.state.board;
        if (board[y][x] === 0) {
            board[y][x] = -1;
            this.setState({
                board
            });
        }
        if (board[y][x] > 0) {
            const shipId: number = board[y][x];
            board[y][x] = -2;

            this.setState((currentState: IState) => {
                return {
                    board,
                    shipStatus: {
                        ...currentState.shipStatus,
                        [shipId]: --currentState.shipStatus[shipId]
                    }
                }
            });
        }
    };

    isGameOver = (): boolean => Object.values(this.state.shipStatus).every((item: number) => item === 0);

    resetGame = (): void => {
        this.setState({
            shipStatus: {
                1: 4,
                2: 4,
                3: 2
            }
        });
        this.initField(constants.DIMENSION);
    };

    render() {
        return (
            <div className="game-container">
                <h1>Battleship the game</h1>

                <div className="game">
                    <div className="score-board">
                        <Score status={this.state.shipStatus}/>
                        <button className="game-btn" onClick={this.resetGame}>Reset</button>
                    </div>
                    <div className="game-board">
                        {
                            this.isGameOver() ?
                                <div className="game-over-banner">Game Over</div> :
                                <GameGrid board={this.state.board} onShot={this.onShot}/>
                        }
                    </div>
                </div>

            </div>
        );
    }
}
