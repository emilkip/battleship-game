import * as React from 'react';
import {constants} from "../configs/constants";
import utils from "../configs/utils";
import Cell from "../components/Cell";

interface IProps {
    board: number[][]
    onShot: (x: number, y: number) => void
}

interface IState {
    board: number[][]
}

export default class GameGrid extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            board: props.board || [],
        };
    }

    static getDerivedStateFromProps(nextProps: IProps, prevState: IState): IState {
        return {
            board: nextProps.board || []
        }
    }

    getColor = (id: number): string => constants.COLORS[id];

    renderBoard = (): JSX.Element[] => (
        this.state.board.map((row: number[], indexY: number) => {
            const uKeyRow: string = utils.generateUKey();
            return (<div key={uKeyRow} className="board-row">{this.renderRow(row, indexY)}</div>)
        })
    );

    renderRow = (row: number[], indexY: number): JSX.Element[] => (
        row.map((cell: number, indexX: number) => {
            const uKeyCell: string = utils.generateUKey();
            return (
                <div key={uKeyCell}>
                    <Cell onShot={this.props.onShot} color={this.getColor(cell)} x={indexX} y={indexY}/>
                </div>
            );
        })
    );

    render() {
        return (
            <div className="board-grid">
                {this.state.board.length ? this.renderBoard() : 'Generating in process. Please wait...' }
            </div>
        );
    }
}
