
export const constants: any = {
    DIMENSION: 10,
    SHIP_NUM: 4,
    SHIPS_TO_GENERATE: ['L','I','B','B'],
    SHIP_TYPES: {
        L: 1,
        I: 2,
        B: 3
    },
    COLORS: {
        '-2': '#f44336',
        '-1': '#9e9e9e',
        '0': '#fff',
        '1': '#3f51b5',
        '2': '#029687',
        '3': '#fec10a'
    }
};
