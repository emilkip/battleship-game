import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Game from './Screens/Game'


ReactDOM.render(
    <Game />,
    document.getElementById('app')
);
